import requests
import pytest
import json
from common import config
from common import credentials


#create new checklist

def test_create_new_checklist():
   URL = config.TRELLO_URL + "/checklists"
   ID = config.ID_CARD
   KEY = credentials.KEY
   TOKEN = credentials.TOKEN
   query = {
      'idCard': ID,
      'key': KEY,
      'token': TOKEN,
      'name': 'testowa checklista'
   }

   response = requests.request(
      "POST",
      URL,
      params=query
   )

   assert response.status_code==200
   response_body = response.json()
   print(response_body['id'])
   def checklist_id():
      checklist_id = response_body['id']


#delete checklist
def delete_checklist(checklist_id):
   URL = config.TRELLO_URL + "/checklists" + checklist_id
   KEY = credentials.KEY
   TOKEN = credentials.TOKEN

   query = {
     'key': KEY,
     'token': TOKEN
   }

   response = requests.request(
      "DELETE",
      URL,
      params=query
)

   print(response.text)


#create board with cleaning

@pytest.fixture()
def board_id():

    url = config.TRELLO_URL_BOARDS

    query = {
        'name': 'board_1',
        'key': credentials.KEY,
        'token': credentials.TOKEN
    }

    response = requests.post(url=url, params=query)

    print(response.content)

    board = json.loads(response.content)
    board_id = board['id']
    yield board_id
    delete_board(response)


def delete_board(update_parameter):
    board = json.loads(update_parameter.content)
    board_id = board['id']
    url = config.TRELLO_URL_BOARDS + '/' + board_id

    query = {
        'key': credentials.KEY,
        'token': credentials.TOKEN
    }

    response = requests.delete(
        url,
        params=query
    )
    print(response.text)
    print(response.status_code)


@pytest.fixture()
def board_id2():

    url = config.TRELLO_URL_BOARDS

    query = {
        'name': 'board_1',
        'key': credentials.KEY,
        'token': credentials.TOKEN
    }

    response = requests.post(url=url, params=query)

    print(response.content)

    board = json.loads(response.content)
    board_id2 = board['id']
    yield board_id2

@pytest.fixture()
def create_list_on_board(board_id2):
    url = config.TRELLO_URL_BOARDS + "/" + board_id2 + "/lists"

    query = {
        'name': 'list_test',
        'key': credentials.KEY,
        'token': credentials.TOKEN
    }

    response = requests.post(url=url, params=query)

    print(response.content)

    list = json.loads(response.content)
    list_id = list['id']
    yield list_id

@pytest.fixture()
def create_card(create_list_on_board):

    url = config.TRELLO_URL_CARDS

    query = {
        'idList': create_list_on_board,
        'key': credentials.KEY,
        'token': credentials.TOKEN
    }

    response = requests.post(url=url, params=query)

    print(response.content)

    card = json.loads(response.content)
    card_id = card['id']
    yield card_id
