import requests
import json
from common import config
from common import credentials

def test_get_the_board_a_checklist_is_on():
    URL = config.TRELLO_URL_CHECKLISTS + '/' + '6290e8cbcea6b823c6fb93fd/board'
    KEY = credentials.KEY
    TOKEN = credentials.TOKEN
    query = {
        'key': KEY,
        'token': TOKEN
}
    response = requests.request(
        "GET",
        URL,
        params=query
)
    assert response.status_code==200
    response_body = response.json()
    print(response_body)