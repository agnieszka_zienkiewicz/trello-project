import requests
import json
from common import config
from common import helpers
from common import credentials


def test_delete_newly_created_checklist():
   URL = config.TRELLO_URL_CHECKLISTS
   ID = config.ID_CARD
   KEY = credentials.KEY
   TOKEN = credentials.TOKEN
   query = {
      'idCard': ID,
      'key': KEY,
      'token': TOKEN,
      'name': 'checklista do usuniecia'
   }

   response = requests.request(
      "POST",
      URL,
      params=query
   )


   assert response.status_code==200
   response_body = response.json()
   id = response_body['id']

   print(id, 'id to DELETE')
   response = requests.request(
       "DELETE",
       config.TRELLO_URL_CHECKLISTS + '/' + id,
       params=query
   )
   print(response.json())
   assert response.status_code == 200