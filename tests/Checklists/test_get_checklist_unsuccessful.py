import requests
import json
from common import config
from common import credentials

#get without token
def test_get_checklist_unsuccessful():
    URL = config.TRELLO_URL_CHECKLISTS + '/' + '6290e7b1751945435ee6d4c2'
    KEY = credentials.KEY
    query = {
        'key': KEY
}
    response = requests.request(
        "GET",
        URL,
        params=query
)
    assert response.status_code==401