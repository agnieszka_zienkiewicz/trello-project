import requests
import json
from common import config
from common import helpers
from common import credentials

def test_get_checkitem_on_checklist():
    URL = config.TRELLO_URL_CHECKLISTS + '/' + '6290e8cbcea6b823c6fb93fd/checkitems'
    ID = config.ID_CARD
    KEY = credentials.KEY
    TOKEN = credentials.TOKEN
    query = {
        'key': KEY,
        'token': TOKEN
}
    response = requests.request(
        "GET",
        URL,
        params=query
)
    assert response.status_code==200
    response_body = response.json()
    print(response_body)