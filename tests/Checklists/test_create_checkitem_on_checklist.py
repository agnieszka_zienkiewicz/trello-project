import requests
from common import config
from common import credentials

checkitemtId = ''

def test_create_checkitem_on_checklist_completed():
    URL = config.TRELLO_URL_CHECKLISTS + '/' + '6290e8cbcea6b823c6fb93fd/checkitems'
    KEY = credentials.KEY
    TOKEN = credentials.TOKEN
    query = {
        'key': KEY,
        'token': TOKEN,
        'name': 'checkitem',
        'pos': 'top',
        'checked' : 'true'
    }
    response = requests.request(
        "POST",
        URL,
        params=query
    )
    assert response.status_code==200
    response_body = response.json()
    assert response_body['name'] == 'checkitem'
    assert response_body['state'] == 'complete'
    global checkitemId
    checkitemId = response_body['id']
    print(response_body['id'])

def test_create_checkitem_on_checklist_incompleted():
    URL = config.TRELLO_URL_CHECKLISTS + '/' + '6290e8cbcea6b823c6fb93fd/checkitems'
    KEY = credentials.KEY
    TOKEN = credentials.TOKEN
    query = {
        'key': KEY,
        'token': TOKEN,
        'name': 'checkitem2',
        'pos': 'bottom',
        'checked' : 'false'
    }
    response = requests.request(
        "POST",
        URL,
        params=query
    )
    assert response.status_code==200
    response_body = response.json()
    assert response_body['state'] == 'incomplete'

def test_create_checkitem_on_checklist_unsuccessful_auth():
    URL = config.TRELLO_URL_CHECKLISTS + '/' + '6290e8cbcea6b823c6fb93fd/checkitems'
    KEY = credentials.KEY
    TOKEN = credentials.TOKEN
    query = {
        'key': KEY,
        'name': 'checkitem2',
        'pos': 'bottom',
        'checked': 'false'
    }
    response = requests.request(
        "POST",
        URL,
        params=query
    )
    assert response.status_code == 401
    assert response.text == "unauthorized permission requested"

def test_create_checkitem_on_checklist_unsuccessful_without_name():
    URL = config.TRELLO_URL_CHECKLISTS + '/' + '6290e8cbcea6b823c6fb93fd/checkitems'
    KEY = credentials.KEY
    TOKEN = credentials.TOKEN
    query = {
        'key': KEY,
        'token': TOKEN,
        'pos': 'bottom',
        'checked': 'false'
    }
    response = requests.request(
        "POST",
        URL,
        params=query
    )
    assert response.status_code == 400
    assert response.text == 'invalid value for name'

def test_delete_checkitem_on_checklist():
    URL = config.TRELLO_URL_CHECKLISTS + '/' + '6290e8cbcea6b823c6fb93fd/checkitems/' + checkitemId
    KEY = credentials.KEY
    TOKEN = credentials.TOKEN
    query = {
        'key': KEY,
        'token': TOKEN
    }
    response = requests.request(
        "DELETE",
        URL,
        params=query
    )

    assert response.status_code == 200
    print(response.text)
