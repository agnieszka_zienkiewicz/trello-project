import requests
import json
from common import config
from common import helpers
from common import credentials

def test_get_checklist():
   URL = config.TRELLO_URL_CHECKLISTS + '/' + '6290e7b1751945435ee6d4c2'
   KEY = credentials.KEY
   TOKEN = credentials.TOKEN
   query = {
      'key': KEY,
      'token': TOKEN
}

   response = requests.request(
      "GET",
      URL,
      params=query
)
   assert response.status_code==200
   response_body = response.json()
   print(response_body['id'])
