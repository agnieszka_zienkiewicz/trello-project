import requests
from common import config
from common import credentials

def test_search_page_data_for_3_first_results():
    URL = config.TRELLO_URL + "/search"
    KEY = credentials.KEY
    TOKEN = credentials.TOKEN
    headers = {
        "Accept": "application/json"
    }
    query = {
        'query': 'To Do',
        'cards_limit': '3',
        'cards_page': '1',
        'partial': 'true',
        'key': KEY,
        'token': TOKEN
    }
    response = requests.request(
        "GET",
        URL,
        headers=headers,
        params=query
    )
    assert response.status_code == 200
    response_body = response.json()
    assert len(response_body['cards']) == 3

