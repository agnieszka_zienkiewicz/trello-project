import requests
from common import config
from common import helpers
from common import credentials
from common.helpers import board_id2
from common.helpers import create_list_on_board
from common.helpers import create_card

checklistId = ''

def test_create_new_checklist(create_card):
   URL = config.TRELLO_URL_CHECKLISTS
   CARD_ID = create_card
   KEY = credentials.KEY
   TOKEN = credentials.TOKEN
   query = {
      'idCard': CARD_ID,
      'key': KEY,
      'token': TOKEN,
      'name': 'testowa checklista',
      'pos' : 'top'
   }

   response = requests.request(
      "POST",
      URL,
      params=query
   )

   assert response.status_code == 200
   response_body = response.json()
   assert response_body['name'] == 'testowa checklista'
   global checklistId
   checklistId = response_body['id']
   print(response_body['id'])


def test_create_new_checklist_unsuccessful_without_idCard():
   URL = config.TRELLO_URL_CHECKLISTS
   KEY = credentials.KEY
   TOKEN = credentials.TOKEN
   query = {
      'key': KEY,
      'token': TOKEN,
      'name': 'testowa checklista',
      'pos' : 'top'
   }

   response = requests.request(
      "POST",
      URL,
      params=query
   )

   assert response.status_code==400
   assert response.text == "invalid value for idCard"

def test_delete_checklist():
   URL = config.TRELLO_URL_CHECKLISTS + '/' + checklistId
   KEY = credentials.KEY
   TOKEN = credentials.TOKEN
   query = {
      'key': KEY,
      'token': TOKEN,
   }

   response = requests.request(
      "DELETE",
      URL,
      params=query
   )

   assert response.status_code == 200




