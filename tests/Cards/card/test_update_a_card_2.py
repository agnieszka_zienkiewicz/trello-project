import requests

from common import credentials
from tests.Cards.Assert_response import assert_response

def test_update_a_card_2():
   url = "https://api.trello.com/1/cards/CZJ3YC5Y"

   headers = {
      "Accept": "application/json"
   }

   query = {
      'key': credentials.KEY,
      'token': credentials.TOKEN,
      'name' : "card_number_2"
   }

   response = requests.request(
      "PUT",
      url,
      headers=headers,
      params=query
   )
   query_body = response.json()
   assert query_body['name'] == 'card_number_2'
   assert_response(response)