import requests

from common import credentials
from tests.Cards.Assert_response import assert_response

def test_update_a_card():
   url = "https://api.trello.com/1/cards/ttEKnEHw"

   headers = {
      "Accept": "application/json"
   }

   query = {
      'key': credentials.KEY,
      'token': credentials.TOKEN,
      'name' : "card_number_1"
   }

   response = requests.request(
      "PUT",
      url,
      headers=headers,
      params=query
   )
   query_body = response.json()
   assert query_body['name'] == 'card_number_1'
   assert_response(response)