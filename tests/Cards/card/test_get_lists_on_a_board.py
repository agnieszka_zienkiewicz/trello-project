import requests

from common import credentials
from tests.Cards.Assert_response import assert_response

def test_get_list_on_a_board():
   url = "https://api.trello.com/1/boards/62b2f16ddb59a207af5ca709/lists"

   query = {
      'key': credentials.KEY,
      'token': credentials.TOKEN
   }

   response = requests.request(
      "GET",
      url,
      params=query
   )
   assert_response(response)
