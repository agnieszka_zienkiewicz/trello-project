import requests
from common import credentials
from tests.Cards.Assert_response import assert_response


def test_create_a_board_CARD():
   url = "https://api.trello.com/1/boards/"

   query = {
       'name': 'board_7',
       'key': credentials.KEY,
       'token': credentials.TOKEN
   }

   response = requests.request(
       "POST",
       url,
       params=query
   )

   assert_response(response)
