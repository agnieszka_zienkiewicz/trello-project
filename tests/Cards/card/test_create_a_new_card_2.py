import requests

from common import credentials
from tests.Cards.Assert_response import assert_response

def test_create_a_new_card():
   url = "https://api.trello.com/1/cards"

   query = {
      'idList': '62b2f16ddb59a207af5ca711',
      'key': credentials.KEY,
      'token': credentials.TOKEN
   }

   response = requests.request(
      "POST",
      url,
      params=query)
   assert_response(response)