import requests

from common import credentials
from tests.Cards.Assert_response import assert_response

def test_create_a_label():
   url = "https://api.trello.com/1/labels"

   query = {
      'name': 'heatr',
      'color': 'pink',
      'idBoard': "62b2f16ddb59a207af5ca709",
      'key': credentials.KEY,
      'token': credentials.TOKEN
   }

   response = requests.request(
      "POST",
      url,
      params=query
   )
   response_body = response.json()
   assert response_body['id'] == '62b2f636522bf552052b197a'
   assert_response(response)