import time

import requests

from common import credentials
from tests.Cards.Assert_response import assert_response

def test_add_and_remove_a_label_to_a_card():
   url = "https://api.trello.com/1/cards/CZJ3YC5Y/idLabels"

   query = {
      'key': credentials.KEY,
      'token': credentials.TOKEN,
      'value': "62b2f636522bf552052b197a"
   }

   response = requests.request(
      "POST",
      url,
      params=query
   )

   remove_url = "https://api.trello.com/1/cards/CZJ3YC5Y/idLabels/62b2f636522bf552052b197a"

   query = {
      'key': credentials.KEY,
      'token': credentials.TOKEN
   }

   response = requests.request(
      "DELETE",
      remove_url,
      params=query
   )

   assert_response(response)

