import requests
from common import credentials
from tests.Cards.Assert_response import assert_response

def test_create_a_label_unsuccesfull():
   url = "https://api.trello.com/1/labels"

   query = {
      'name': 'heatr',
      'color': 'pink',
      'idBoard': "62b2f16ddb59a207af5ca709",
      'key': credentials.KEY,
   }

   response = requests.request(
      "POST",
      url,
      params=query
   )

   assert_response(response, 401)