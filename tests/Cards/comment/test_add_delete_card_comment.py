
#create
import time
import requests
from common import credentials
from tests.Cards.Assert_response import assert_response

def test_add_delete_card_comment():
   creation_url = "https://api.trello.com/1/cards/CZJ3YC5Y/actions/comments"

   query = {
      'text': 'I will be gone soon!',
      'key': credentials.KEY,
      'token': credentials.TOKEN
   }

   creation_response = requests.request(
      "POST",
      creation_url,
      params=query
   )
   assert_response(creation_response)

   creation_response_body = creation_response.json()
   created_comment_id = creation_response_body["id"]
   data = creation_response_body["data"]
   text = data["text"]
   assert creation_response_body["data"]["text"] == 'I will be gone soon!'


   delete_url = f"https://api.trello.com/1/cards/CZJ3YC5Y/actions/{created_comment_id}/comments"

   query = {
      'key': credentials.KEY,
      'token': credentials.TOKEN
   }

   delete_response = requests.request(
      "DELETE",
      delete_url,
      params=query
   )
   assert_response(delete_response)
