import requests

from common import credentials
from tests.Cards.Assert_response import assert_response

def test_delete_a_comment_on_a_card():
   url = "https://api.trello.com/1/cards/CZJ3YC5Y/actions/62b36ebfb4b55384aa2881d9/comments"

   query = {
      'key': credentials.KEY,
      'token': credentials.TOKEN
   }

   response = requests.request(
      "DELETE",
      url,
      params=query
   )
   assert_response(response)