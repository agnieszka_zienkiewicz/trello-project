import requests

from common import credentials
from tests.Cards.Assert_response import assert_response

def test_add_a_new_commnet_to_a_card():
   url = "https://api.trello.com/1/cards/CZJ3YC5Y/actions/comments"


   query = {
      'text': 'Hello there! General kenobi! <3',
      'key': credentials.KEY,
      'token': credentials.TOKEN
   }

   response = requests.request(
      "POST",
      url,
      params=query
   )
   query_body = response.json()
   assert query_body["data"]['text'] == 'Hello there! General kenobi! <3'
   assert_response(response)