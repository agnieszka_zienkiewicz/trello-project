def assert_response(response, expected_status_code=200):
    assert response.status_code == expected_status_code, "Wrong status code\n" \
                                                     f"Expected: {expected_status_code}\n" \
                                                     f"Current: {response.status_code}"
