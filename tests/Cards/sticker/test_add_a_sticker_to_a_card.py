import requests

from common import credentials
from tests.Cards.Assert_response import assert_response

def test_add_a_sticker_to_a_card():
   url = "https://api.trello.com/1/cards/ttEKnEHw/stickers"

   query = {
      'image': 'taco-reading',
      'top': '0',
      'left': '65.15748031496062',
      'zIndex': '1' ,
      'key': credentials.KEY,
      'token': credentials.TOKEN
   }

   response = requests.request(
      "POST",
      url,
      params=query
   )
   response_body = response.json()
   assert response_body['image'] == 'taco-reading'
   assert_response(response)