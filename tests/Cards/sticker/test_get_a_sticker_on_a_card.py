import requests

from common import credentials
from tests.Cards.Assert_response import assert_response

def test_get_a_sticker_on_a_card():
   url = "https://api.trello.com/1/cards/ttEKnEHw/stickers"

   query = {
      'key': credentials.KEY,
      'token': credentials.TOKEN
   }

   response = requests.request(
      "GET",
      url,
      params=query
   )
   assert_response(response)