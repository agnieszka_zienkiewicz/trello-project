import requests

from common import credentials
from tests.Cards.Assert_response import assert_response

def test_delete_a_sticker_on_a_card():
   url = "https://api.trello.com/1/cards/ttEKnEHw/stickers/62a73ea431ddd03dc11fd6db"

   query = {
      'key': credentials.KEY,
      'token': credentials.TOKEN
   }

   response = requests.request(
      "DELETE",
      url,
      params=query
   )
   assert_response(response)