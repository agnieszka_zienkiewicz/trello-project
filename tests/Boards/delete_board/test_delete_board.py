import json
import requests
from common import config
from common import credentials
from common.helpers import board_id


def test_delete_board():
    url = config.TRELLO_URL_BOARDS + '/' + config.ID_BOARD_TO_DELETE

    query = {
        'key': credentials.KEY,
        'token': credentials.TOKEN
    }

    response = requests.delete(
        url,
        params=query
    )

    assert response.status_code == 200