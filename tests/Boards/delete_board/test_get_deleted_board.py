import requests
from common import config
from common import credentials


def test_get_deleted_board():
    url = config.TRELLO_URL_BOARDS + '/' + config.ID_BOARD_TO_DELETE

    headers = {
        "Accept": "application/json"
    }
    query = {
        'key': credentials.KEY,
        'token': credentials.TOKEN
    }
    response = requests.get(
        url,
        headers=headers,
        params=query
    )

    assert response.status_code == 404
    assert response.content == b'The requested resource was not found.'

