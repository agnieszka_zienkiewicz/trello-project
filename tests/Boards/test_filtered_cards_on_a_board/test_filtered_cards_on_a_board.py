import requests
import json
from common import config
from common.helpers import board_id
from common import credentials


def create_list(board_id):
    url = config.TRELLO_URL_LISTS

    query = {
        'name': 'list_1',
        'idBoard': board_id,
        'key': credentials.KEY,
        'token': credentials.TOKEN
    }

    response = requests.post(url=url, params=query)

    print(response.content)

    list_content = json.loads(response.content)
    list_id = list_content['id']
    return list_id


def create_cards(list_id):
    array = []
    for i in range(20):
        name = 'card' + str(i)
        card_id = create_card(name, list_id)
        create_card(name, list_id)
        array.append(card_id)
    return array


def create_card(name, list_id):
    url = config.TRELLO_URL_CARDS

    headers = {
        "Accept": "application/json"
    }

    query = {
        'name': name,
        'idList': list_id,
        'key': credentials.KEY,
        'token': credentials.TOKEN
    }

    response = requests.request(
        "POST",
        url,
        headers=headers,
        params=query
    )

    card_content = json.loads(response.content)
    card_id = card_content['id']
    return card_id


def close_cards(array):
    closed_card_id = []
    x = int(len(array) / 2)
    for i in range(1, x):
        card_id = array[i]
        url = config.TRELLO_URL_CARDS + '/' + card_id

        headers = {
            "Accept": "application/json"
        }

        query = {
            'closed': 'true',
            'key': credentials.KEY,
            'token': credentials.TOKEN
        }

        response = requests.request(
            "PUT",
            url,
            headers=headers,
            params=query
        )
        closed_card_content = json.loads(response.content)
        closed_card_id.append(closed_card_content['id'])
    return closed_card_id


def test_filtered_cards(board_id):
    url = config.TRELLO_URL_BOARDS + "/" + board_id + "/cards/closed"

    list_id = create_list(board_id)
    array = create_cards(list_id)
    close_cards(array)
    query = {
        'key': credentials.KEY,
        'token': credentials.TOKEN
    }

    response = requests.request(
        "GET",
        url,
        params=query
    )

    print(response.text)
    response_object = json.loads(response.content)
    assert len(response_object) == 9

    for i in range(len(response_object)):
        assert response_object[i]['closed'] is True

