import json
import requests
from common import config
from common.helpers import board_id
from common import credentials


def test_update_board(board_id):
    url = config.TRELLO_URL_BOARDS + '/' + board_id

    query = {
        'name': config.BOARD_UPDATE_NAME,
        'key': credentials.KEY,
        'token': credentials.TOKEN
    }

    response = requests.put(
        url,
        params=query
    )

    assert response.status_code == 200

    response_object = json.loads(response.content)
    assert response_object['id'] == board_id
    assert response_object['name'] == 'board_2'
