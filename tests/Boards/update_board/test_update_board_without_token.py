import json
import requests
from common import config
from common.helpers import board_id
from common import credentials


def test_update_board_without_key(board_id):
    url = config.TRELLO_URL_BOARDS + '/' + board_id

    query = {
        'name': 'board_2',
        'token': credentials.TOKEN
    }

    response = requests.put(
        url,
        params=query
    )

    assert response.status_code == 401
    assert response.content == b'invalid key'