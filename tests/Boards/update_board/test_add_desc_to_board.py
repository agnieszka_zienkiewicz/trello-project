import json
import requests
from common import config
from common.helpers import board_id
from common import credentials


def test_add_a_desc_to_the_board(board_id):
    url = config.TRELLO_URL_BOARDS + '/' + board_id

    query = {
        'desc': config.DESC,
        'key': credentials.KEY,
        'token': credentials.TOKEN
    }
    response = requests.put(url=url, params=query)
    assert response.status_code == 200

    response_object = json.loads(response.content)
    assert response_object['desc'] == 'This is my first board.'



