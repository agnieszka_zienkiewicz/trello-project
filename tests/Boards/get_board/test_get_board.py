import requests
import json
from common import credentials
from common import config
from common.helpers import board_id


def test_get_board(board_id):

    url = config.TRELLO_URL_BOARDS + '/' + board_id
    headers = {
        "Accept": "application/json"
    }
    query = {
        'key': credentials.KEY,
        'token': credentials.TOKEN
    }
    response = requests.get(
        url,
        headers=headers,
        params=query
    )

    assert response.status_code == 200
    response_object = json.loads(response.content)
    assert response_object['name'] == 'board_1'
    assert response_object['id'] == board_id

