import requests
from common import config
from common import credentials


def test_create_board_without_board_name_unsuccessful():
    url = config.TRELLO_URL_BOARDS

    query = {
        'key': credentials.KEY,
        'token': credentials.TOKEN
    }

    response = requests.post(url=url, params=query)
    assert response.status_code == 400
    assert response.content == b'invalid value for name'