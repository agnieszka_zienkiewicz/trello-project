import json
import requests
from common import config
from common import credentials


def test_create_board_successful():
    url = config.TRELLO_URL_BOARDS

    query = {
        'name': 'board_1',
        'key': credentials.KEY,
        'token': credentials.TOKEN
    }

    response = requests.post(url=url, params=query)
    assert response.status_code == 200

    response_object = json.loads(response.content)
    assert response_object['name'] == 'board_1'

