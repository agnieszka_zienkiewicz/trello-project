import requests
from common import config
from common import credentials


def test_create_board_without_key_unsuccessful():
    url = config.TRELLO_URL_BOARDS

    query = {
        'name': 'board_1',
        'token': credentials.TOKEN
    }

    response = requests.post(url=url, params=query)
    assert response.status_code == 401
    assert response.content == b'invalid key'